import React from 'react';
import Router from './src/scenes/Router.js';

const App = () => <Router />;

export default App;
