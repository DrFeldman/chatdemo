import React from 'react';
import {View, StatusBar} from 'react-native';
import PropTypes from 'prop-types';
import styles, {colors} from '../config/styles';
import Header from './Header';
import Loader from './Loader';

const Scene = props => {
  const {children, title, loading} = props;
  return (
    <View style={styles.common.rootContainer}>
      <StatusBar backgroundColor={colors.black} barStyle="light-content" />
      <Loader visible={loading} />
      <Header title={title} />
      <View style={styles.common.rootView} {...props}>
        {children}
      </View>
    </View>
  );
};

Scene.propTypes = {
  children: PropTypes.node,
  title: PropTypes.string,
  loading:PropTypes.bool,
};

Scene.defaultProps = {
  children: null,
  header: '',
  loading:false,
};

export default Scene;
