import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import PropTypes from 'prop-types';
import styles, {colors} from '../config/styles';
import strings from '../config/localization';
import avatar from '../assets/images/avatar.jpg';
import avatar_2 from '../assets/images/avatar_2.jpg';

const MessageView = ({message, isIncoming, onPress}) => {
  const color = isIncoming ? colors.grey : colors.white;
  const name = isIncoming ? strings.name_2 : strings.name;
  const image = isIncoming ? avatar_2 : avatar;
    const renderMessageContent = () => {
      switch (message.type) {
        case 'image':
          return <ImageMessageContent onPress={onPress} message={message} />;
        default:
          return (
            <View style={[styles.chat.messageTextContainer, {backgroundColor: color}]}>
              <Text style={styles.chat.messageText}>{message.text}</Text>
            </View>);
      }
    };

  return (
    <View style={styles.chat.messageContainer}>
        <View style={{flexDirection:'row', alignItems:'center'}}>
            <Image source={image} style={styles.common.headerImage} />
            <Text style={styles.chat.userNameText}>{name}</Text>
        </View>
        <View style={{marginLeft:55, maxWidth:'80%'}}>
            {renderMessageContent()}
            <Text style={styles.chat.messageTimeText}>{new Date(message.createdAt).getHours()}:{new Date(message.createdAt).getMinutes()}</Text>
        </View>
    </View>
  );
};

MessageView.propTypes = {
  message: PropTypes.object.isRequired,
  isIncoming: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
};

MessageView.defaultProps = {
  onPress: () => {},
};

export default MessageView;

const ImageMessageContent = ({message}) => (
    <Image
      style={styles.chat.messageImage}
      source={{uri: message.file}}
    />
);

ImageMessageContent.propTypes = {
  message: PropTypes.object.isRequired,
};
