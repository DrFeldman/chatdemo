import React from 'react';
import { Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import LinearGradient from 'react-native-linear-gradient';
import PropTypes from 'prop-types';
import styles, {colors} from '../config/styles';
import ava from '../assets/images/avatar.jpg';

const Header = props => {
  const {avatar} = props;
  return (
    <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={[colors.pink, colors.pinkOrange, colors.orange]} style={styles.common.headerContainer}>
        <Icon name="ios-arrow-back" size={30} color={colors.white} />
      <Image source={avatar} style={styles.common.headerImage} />
    </LinearGradient>
  );
};

Header.propTypes = {
  title: PropTypes.string,
  avatar: PropTypes.node,
  backButton: PropTypes.func,
};
Header.defaultProps = {
  title: '',
  avatar: ava,
  backButton: () => {},
};

export default Header;
