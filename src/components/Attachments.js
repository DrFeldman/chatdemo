import React from 'react';
import {View} from 'react-native';
import PropTypes from 'prop-types';
import styles from '../config/styles';
import Button from './Button';

const Attacments = ({handleImagePress, handleGalleryPress}) => {
  return (
    <View style={styles.chat.attachmentsContainer}>
        <Button onPress={handleImagePress} icon="image" />
        <Button onPress={handleGalleryPress} icon="insert-drive-file" />
        <Button onPress={handleImagePress} icon="mic" />
        <Button onPress={handleImagePress} icon="gif" />
    </View>
  );
};

Attacments.propTypes = {
  handleImagePress: PropTypes.func,
  handleGalleryPress: PropTypes.func,
};

Attacments.defaultProps = {
  handleImagePress: () => {},
  handleGalleryPress: () => {},
};

export default Attacments;
