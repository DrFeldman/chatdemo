import React from 'react';
import { TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import styles, {colors} from '../config/styles';

const Button = props => {
  const {icon} = props;
  return (
    <TouchableOpacity  style={styles.common.buttonContainer} {...props}>
        <Icon name={icon} size={25} color={colors.white} />
    </TouchableOpacity>
  );
};

Button.propTypes = {
    icon: PropTypes.string,
};
Button.defaultProps = {
    icon: '',
};

export default Button;
