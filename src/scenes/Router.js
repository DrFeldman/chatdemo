import React from 'react';
import {Router, Scene, ActionConst} from 'react-native-router-flux';
import Chat from './Chat/Chat';

const RouterComponent = () => (
  <Router>
    <Scene key="root" hideNavBar>
      <Scene type={ActionConst.RESET} key="Chat" component={Chat} />
    </Scene>
  </Router>
);
export default RouterComponent;
