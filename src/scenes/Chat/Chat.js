import React, {Component} from 'react';
import { View, TouchableOpacity, ScrollView, Keyboard, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import ImagePicker from 'react-native-image-picker';
import Permissions from 'react-native-permissions';
import Scene from '../../components/Scene';
import Messages from '../../components/Message';
import Attachments from '../../components/Attachments';
import strings from '../../config/localization';
import styles, { colors } from '../../config/styles';
import {getChat, postMessage, uploadFile} from '../../actions/chat';
import {getPermission} from '../../config/helpers';

class ChatScene extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      messages: [],
      message:'',
      attachments:false,
    };
  }

  componentWillMount() {
    this.setState({loading:true});
    getChat()
      .then(data => this.setState({messages: data.messages}))
      .catch(err => console.warn('Error:', err))
      .finally(()=>this.setState({loading:false}));

    this.keyboardDidShowListener =
      Keyboard.addListener(
        'keyboardDidShow',
        () => this.scrollView.scrollToEnd({animated: true})
      );
  }

  componentWillUnmount () {
    this.keyboardDidShowListener.remove();
  }

  isMessageIncoming(message) {
    return message.user._id === 'Denys' || message.user._id === undefined;
  }

  handleSendMessage(message){
    const {messages} = this.state;
    postMessage({attachments: [], text:message})
      .then((res) => this.setState({message: '', messages: [...messages, {text:message, user:{_id:'Denys'}, createdAt:new Date() }]}));
  }

  handleSendAttachment(file){
    const {messages} = this.state;
    if (file.error || file.didCancel) {return;}
    const data = new FormData();
    data.append('file', file.uri, 'image.png');
    uploadFile(data)
      .then((res) => this.setState({message: '', messages: [...messages, { file:file.uri, type:'image', user:{_id:'Denys'}, createdAt:new Date() }], attachments:false }));
  }

  checkPermission(type){
    Permissions.request(getPermission(type));
  }

  render() {
    const {loading, messages, message, attachments} = this.state;
    return (
      <Scene loading={loading}>
        <ScrollView
          style={{flex: 1}}
          ref={ref => (this.scrollView = ref)}
          contentContainerStyle={{padding: 10, paddingTop: 70}}
          onContentSizeChange={() => this.scrollView.scrollToEnd({animated: true})}
          >
          {messages.map(item => (
            <Messages
              key={item._id}
              message={item}
              isIncoming={this.isMessageIncoming(item)}
              onPress={()=>{}}
            />
          ))}
        </ScrollView>

        <View style={styles.chat.messageInputContainer}>
          <TouchableOpacity
            style={styles.chat.messageInputIconButton}
            onPress={()=>this.setState({attachments:!attachments})}
          >
            <Icon name="paperclip" size={25} color={colors.darkGrey} />
          </TouchableOpacity>
          <TextInput
            value={message}
            onChangeText={input => this.setState({message:input})}
            style={styles.chat.messageTextInput}
            placeholder={strings.sendMessage}
          />
          <TouchableOpacity
            style={styles.chat.messageInputIconButton}
            onPress={() => this.handleSendMessage(message)}
          >
            <Icon name="send" size={23} color={colors.darkGrey} />
          </TouchableOpacity>
        </View>
        {attachments && (<Attachments
          handleImagePress={() => Permissions.request(getPermission('camera')).then(per => {if (per === 'denied') {return this.checkPermission('camera');} ImagePicker.launchCamera({quality:0.3}, response => this.handleSendAttachment(response));})}
          handleGalleryPress={() => Permissions.request(getPermission('photo')).then(per => {if (per === 'denied') {return this.checkPermission('photo');} ImagePicker.launchImageLibrary({quality:0.3}, response => this.handleSendAttachment(response));})}
        />)}
      </Scene>
    );
  }
}

export default ChatScene;
