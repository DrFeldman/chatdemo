import axios from 'axios';
import remote from '../config/constants';

export const getChat = (id = '5ddd1b9055ed67349487d33e') =>
  axios
    .get(remote.api + `chats/${id}`, remote.params)
    .then(response => Promise.resolve(response.data))
    .catch(() => Promise.resolve(null));

export const postMessage = (id = '5ddd1b9055ed67349487d33e', data) =>
  axios
    .post(remote.api + `chats/${id}`, data, remote.params)
    .then(response => Promise.resolve(response.data))
    .catch(() => Promise.resolve(null));

export const uploadFile = (data) =>
  axios
    .post(remote.api + 'upload/', data, remote.params)
    .then(response => Promise.resolve(response.data))
    .catch(() => Promise.resolve(null));
