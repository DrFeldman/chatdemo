import {Platform, StyleSheet} from 'react-native';

export const IOSpadding = Platform.OS === 'ios' ? 23 : 0;

export const Shadow = Platform.select({
  ios: {
    shadowOpacity: 0.3,
    shadowRadius: 5,
    shadowOffset: {
      height: 0,
      width: 0,
    },
  },
  android: {elevation: 5},
});

export const colors = {
  black: 'rgb(7,5,4)',
  white: 'rgb(255,255,255)',
  dark: 'rgb(20,18,17)',
  pink: 'rgb(253,17,153)',
  pinkOrange: 'rgb(253,57,99)',
  orange: 'rgb(253,100,65)',
  ligth:'rgb(240,246,249)',
  grey:'rgb(213,223,235)',
  darkGrey:'rgb(53,48,48)',
};

export const common = StyleSheet.create({
  rootContainer: {
    flex: 1,
    backgroundColor: colors.white,
  },
  rootView: {
    flex: 1,
    backgroundColor: colors.ligth,
  },
  preloaderContainer:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
  },
  headerContainer: {
    height: 45 + IOSpadding,
    paddingTop: IOSpadding,
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  headerImage: {
    width: 40,
    height: 40,
    borderRadius: 20,
  },
  buttonContainer:{
    width:40,
    height:40,
    borderRadius:20,
    backgroundColor:colors.darkGrey,
    alignItems:'center',
    justifyContent:'center',
  },
});

export const chat = StyleSheet.create({
  messageContainer: {
    flex: 1,
  },
  messageTextContainer: {
    backgroundColor: colors.white,
    paddingHorizontal: 10,
    borderRadius: 10,
    flexDirection: 'row',
    flex:1,
  },
  userNameText:{
    color: colors.pinkOrange,
    fontSize: 16,
    fontWeight:'bold',
    marginHorizontal:15,
  },
  messageText: {
    color: colors.black,
    fontSize: 16,
    paddingVertical: 5,
  },
  messageImage: {
    width: 200,
    height: 200,
    marginVertical: 10,
    borderRadius: 15,
  },
  messageInputContainer: {
    flexDirection: 'row',
    borderTopWidth: 1,
    borderColor: colors.grey,
    alignItems: 'center',
  },
  messageTextInput: {
    marginVertical: 5,
    padding: 1,
    flex: 1,
  },
  messageInputIconButton: {
    width: 54, height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  messageTimeText:{
    alignSelf:'flex-end',
    color:colors.darkGrey,
    fontSize:12,
    marginTop:5,
  },
  attachmentsContainer:{
    flexDirection: 'row',
    justifyContent:'space-around',
    borderTopWidth: 1,
    borderColor: colors.grey,
    alignItems: 'center',
    paddingVertical:5,

  },
});

export default {common, chat};
