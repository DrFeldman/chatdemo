import LocalizedStrings from 'react-native-localization';

const strings = new LocalizedStrings({
  ru: {
    name:'Верка Сердючка',
    name_2:'Валентин Стрыкало',
    sendMessage:'Сообщение...',
  },
  en: {
    name:'Verka Serduchka',
    name_2:'Valentin Strykalo',
    sendMessage:'Message...',
},
});

export default strings;
