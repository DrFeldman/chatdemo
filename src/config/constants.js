const config = {
  api: 'https://test7.gurucan.ru/api/',
  params: {
    headers: {
      Cookie:
        'jwt_token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzRiM2M3OWM3OWU2MjJiN2Y2YzE5Y2MiLCJwYXNzd29yZCI6ImU3ZDYxOTI5MDBhZmVmNDIzZDkwN2ExOTVhOTA5ZmU2ODRhYWI0MDciLCJlbWFpbCI6ImpkcG9ub21hcmV2K3Rlc3Q3QGdtYWlsLmNvbSIsImlhdCI6MTU2OTkxMzA4N30.iRDThkZcygatqxveXiADUkV5g4YM1_gtvw9RyzhOOXM',
    },
  },
};
export default config;
