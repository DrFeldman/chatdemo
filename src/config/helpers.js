import { Platform } from 'react-native';
import {PERMISSIONS} from 'react-native-permissions';

export const getPermission = (type = 'location') => {
    switch (type) {
      case 'camera': return   Platform.select({
        ios:PERMISSIONS.IOS.CAMERA,
        android:PERMISSIONS.ANDROID.CAMERA,
      });
      case 'photo': return   Platform.select({
        ios:PERMISSIONS.IOS.PHOTO_LIBRARY,
        android:PERMISSIONS.ANDROID.WRITE_EXTERNAL_STORAGE,
      });
      default: return   Platform.select({
        ios:PERMISSIONS.IOS.LOCATION_WHEN_IN_USE,
        android: PERMISSIONS.ANDROID.ACCESS_FINE_LOCATION,
      });
    }
  };
